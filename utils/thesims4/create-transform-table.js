const fs = require('fs-extra');
const csvParse = require('csv-parse');
const xmlParse = require('fast-xml-parser');

var strings = {};
var transformTable = {};

async function main() {
    console.log("Running…");
    var stringsJson = await fs.readFile("dist/Strings_ENG_US.json", 'utf8');
    stringsJson = JSON.parse(stringsJson);
    for (var e in stringsJson["Entries"]) {
        strings[stringsJson["Entries"][e]["Key"]] = stringsJson["Entries"][e]["Value"];
    }
    //console.log(strings);
    var index = await getIndex();
    //console.log(index);
    for (var i in index) {
        var url = index[i][4].replace(/\\/g, "/");
        //console.log(url);
        var simsXml = await fs.readFile("dist/TS4xml/" + url, 'utf8');
        var simsJson = xmlParse.parse(simsXml, {
            attributeNamePrefix: "@_",
            attrNodeName: "attr",
            textNodeName: "#text",
            ignoreAttributes: false,
            numParseOptions: {
                hex: false,
            },
        });
        var displayNameId = undefined;
        if (simsJson["I"] && simsJson["I"]["T"]) {
            
            for (var n in simsJson["I"]["T"]) {
                var targetNode;
                try {
                    targetNode = simsJson["I"]["T"][n]["attr"]["@_n"];
                } catch (e) {

                }
                if (targetNode == "display_name") {
                    displayNameId = simsJson["I"]["T"][n]["#text"];
                }
            }
        }
        //console.log(displayNameId);
        if (displayNameId && strings[displayNameId]) {
            console.log(strings[displayNameId]);
            transformTable[index[i][1]] = strings[displayNameId];
        }
    }
    await fs.writeFile("transform-table.json", JSON.stringify(transformTable));
    console.log("Done.");
}


const getIndex = async () => {
    records = []
    const parser = fs
        .createReadStream(`dist/index.tsv`)
        .pipe(csvParse({
            delimiter: "\t"
        }));
    for await (const record of parser) {
        records.push(record)
    }
    return records
}

main();