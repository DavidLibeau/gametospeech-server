const fs = require('fs-extra')

var extensions = ["tdesc", "tdescfrag"];

var merge = '<?xml version="1.0" encoding="iso-8859-1"?>\r\n<tdesc>';

async function manageDir(path) {
    var files = await fs.readdir(path);
    for (var f in files) {
        await managefile(files[f], path);
    }
}

async function managefile(file, path) {
    if (file.slice(0, 1) != ".") {
        if (file.endsWith("." + extensions[0]) || file.endsWith("." + extensions[1])) {
            var data = await fs.readFile(path + "/" + file, 'utf8');
            merge += data.replace('<?xml version="1.0" encoding="iso-8859-1"?>\r\n\r\n', "");;
        } else {
            await manageDir(path + "/" + file);
        }
    }
}

async function main() {
    console.log("Running…");
    await manageDir("dist");
    await fs.writeFile("merge.xml", merge+"</tdesc>");
    console.log("Done.");
}

main();