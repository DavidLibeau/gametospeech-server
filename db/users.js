var records = [
  {
    id: 1,
    username: 'david',
    password: 'test',
    displayName: 'David',
    emails: [{
      value: 'contact@davidlibeau.fr'
        }],
    login_key: undefined,
    connected_to: []
    }
];

// login_key: {value: undefined, expired_at: undefined}
// connected_to: [{game: undefined, access_token: undefined}]

exports.findById = function (id, cb) {
  process.nextTick(function () {
    var idx = id - 1;
    if (records[idx]) {
      cb(null, records[idx]);
    } else {
      cb(new Error('User #' + id + ' does not exist'));
    }
  });
}

exports.findByUsername = function (username, cb) {
  process.nextTick(function () {
    for (var i = 0, len = records.length; i < len; i++) {
      var record = records[i];
      if (record.username === username) {
        return cb(null, record);
      }
    }
    
    return cb(new Error('User with username ' + username + ' does not exist'));
  });
}

exports.findByLoginKey = function (loginKey, cb) {
  process.nextTick(function () {
    for (var i = 0, len = records.length; i < len; i++) {
      var record = records[i];
      if (record.login_key && record.login_key.value == loginKey) {
        return cb(null, record);
      }
    }
    return cb(null, null);
  });
}


exports.findByaccessToken = function (accessToken, gameKey) {
  return new Promise((resolve, reject) => {
    for (var i = 0, len = records.length; i < len; i++) {
      var record = records[i];
      if (record.connected_to.length > 0){
        for (var c in record.connected_to) {
          if (record.connected_to[c].access_token == accessToken && record.connected_to[c].game == gameKey) {
            return resolve(record);
          }
        }
      }
    }
    return reject();
  });
}