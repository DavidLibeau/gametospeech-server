var records = [
  {
    id: 1,
    key: 'unity',
    name: 'Unity simple prototype',
    require_auth: false
  },
  {
    id: 2,
    key: 'minecraft-fabricmod',
    name: 'Minecraft using Fabric mod',
    require_auth: true
  },
  {
    id: 3,
    key: 'unity-playfab',
    name: 'Unity Playfab prototype',
    require_auth: false
  },
  {
    id: 4,
    key: 'thesims4-mod',
    name: 'The Sims 4 mod',
    require_auth: false
  },
];

// login_key: {value: undefined, expired_at: undefined}
// connected_to: [{game: undefined, access_token: undefined}]

exports.findById = function (id, cb) {
  process.nextTick(function () {
    var idx = id - 1;
    if (records[idx]) {
      cb(null, records[idx]);
    } else {
      cb(new Error('Game #' + id + ' does not exist'));
    }
  });
}

exports.findByKey = function (key) {
  return new Promise((resolve, reject) => {
    for (var i = 0, len = records.length; i < len; i++) {
      var record = records[i];
      if (record.key === key) {
        return resolve(record);
      }
    }
    return reject(new Error('Game  with key ' + key + ' does not exist'));
  });
}