gamesLib["thesims4-mod"].transformDataToDescription = function (data) {
    if (data.npc == "False" && data.type == "interaction") {
        var action = "";
        if (transformTable[data.action]) {
            action = transformTable[data.action]
        } else {
            action = data.action.replace(/_/g, " ");
        }
        var target = "";
        if (transformTable[data.target]) {
            target = transformTable[data.target]
        } else {
            target = data.target.split(":")[0].replace(/_/g, " ").replace(/\d/g, "");
        }
        if(target == "None"){
            target = "";
        }else{
            target = " on " + target;
        }
        var description = data.character + " " + action + target;
        console.log(description);
        return description;
    }
    return undefined;
}

var transformTable = {};
fetch("/thesims4-transform-table-en.json")
    .then(response => response.json())
    .then(data => {
        console.log("thesims4-transform-table-en.json loaded with "+Object.keys(data).length+" objects");
        transformTable = data;
    });