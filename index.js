const {
  v4: uuidv4
} = require('uuid');
var moment = require('moment');
var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var db = require('./db');

console.log("Launching GameToSpeech server…");

// passport setup
passport.use(new Strategy(
  function (username, password, cb) {
    db.users.findByUsername(username, function (err, user) {
      if (err) {
        return cb(err);
      }
      if (!user) {
        return cb(null, false);
      }
      if (user.password != password) {
        return cb(null, false);
      }
      return cb(null, user);
    });
  }));

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  db.users.findById(id, function (err, user) {
    if (err) {
      return cb(err);
    }
    cb(null, user);
  });
});

// express + socket.io setup
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
http.listen(3000);

app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static('games_lib'))

app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.get('/',
  function (req, res) {
    res.render('home', {
      user: req.user
    });
  });

app.get('/login',
  function (req, res) {
    console.log("login");
    console.log(req.headers);
    console.log(req.query);
    console.log(req.params);
    console.log(req.body);
    res.render('login');
  });

app.post('/login',
  passport.authenticate('local', {
    failureRedirect: '/login'
  }),
  function (req, res) {
    res.redirect('/');
  });

app.get('/logout',
  function (req, res) {
    req.logout();
    res.redirect('/');
  });

app.use('/debug',
  function (req, res) {
    console.log(req.headers);
    console.log(req.query);
    console.log(req.params);
    console.log(req.body);
    res.send('OK');
  });

app.get('/profile',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    res.render('profile', {
      user: req.user
    });
  });

app.get('/new_login_key',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    req.user.login_key = {
      value: uuidv4(),
      expired_at: moment().add(5, 'minutes').format()
    };
    res.redirect('profile');
  });

app.get('/delete_login_key',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    req.user.login_key = undefined;
    res.redirect('profile');
  });

app.get('/delete_connected_game',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    if (!req.query.access_token) {
      res.status(500).json({
        error: 'access_token required.'
      })
    }

    for (var c in req.user.connected_to) {
      if (req.user.connected_to[c].access_token == req.query.access_token) {
        req.user.connected_to.splice(c, 1);
      }
    }
    res.redirect('profile');
  });

app.get('/connect',
  function (req, res) {
    db.users.findByLoginKey(req.query.login_key, function (err, user) {
      if (err) {
        res.status(500).json({
          error: err
        })
      }
      if (user && moment(user.login_key.expired_at).isAfter()) {
        var access_token = uuidv4();
        var connected = false;
        for (var c in user.connected_to) {
          if (user.connected_to[c].game == req.query.game) {
            connected = true;
            user.connected_to[c].access_token = access_token;
          }
        }
        if (!connected) {
          connected = true;
          user.connected_to.push({
            game: req.query.game,
            access_token: access_token
          });
        }
        user.login_key = undefined;
        res.json({
          user_id: user.username,
          access_token: access_token
        });
      } else {
        res.status(404).json({
          error: 'User or login_key not found.'
        })
      }
    });
  });

app.get('/listen/:channel', (req, res) => {
  res.render('listen', {
    channel: req.params.channel
  });
});

var channels = {};

function initChannel(channelName) {
  channels[channelName] = {};
  channels[channelName]['socket'] = io.of('/' + channelName);
  channels[channelName]['socket'].on('connection', socket => {
    console.log('Socket connected on channel ' + channelName);
  });
}

app.use('/:gameId/:channel',
  async (req, res, next) => {
    //checking game
    try {
      var game = await db.games.findByKey(req.params.gameId);
    } catch (err) {
      return res.status(404).json({
        error: 'Game not found.'
      });
    }
    //checking data
    var data = undefined;
    if (req.query.data) {
      try {
        data = JSON.parse(req.query.data);
      } catch (error) {}
    } else if (req.body) {
      if (typeof req.body === 'object') {
        data = req.body;
      } else {
        try {
          data = JSON.parse(req.query.data);
        } catch (error) {}
      }
    }
    if (!data) {
      return res.status(500).json({
        error: 'Data not found.'
      });
    }
    data.game = game.key;
    //if auth is required
    if (game.require_auth) {
      var accessToken = undefined;
      //get accessToken from OAuth Authorization Bearer
      if (req.headers.authorization) {
        if (req.headers.authorization.indexOf("Bearer ") == 0) {
          accessToken = req.headers.authorization.slice(7);
        }
      }
      //get accessToken from GameToSpeech-auth header
      if (!accessToken && req.headers["GameToSpeech-auth"]) {
        accessToken = req.headers["GameToSpeech-auth"];
      }
      //get accessToken from headers
      if (!accessToken && req.headers["GameToSpeech"]) {
        accessToken = req.headers["GameToSpeech"];
      }
      //get accessToken from query
      if (!accessToken && req.query.access_token) {
        accessToken = req.query.access_token;
      }
      //get accessToken from params
      if (!accessToken && req.params.access_token) {
        accessToken = req.params.access_token;
      }
      //get accessToken from data
      if (!accessToken && data.access_token) {
        accessToken = data.access_token;
      }
      if (accessToken) {
        //check access token in db
        try {
          var user = await db.users.findByaccessToken(accessToken, game.key);
        } catch (err) {
          return res.status(401).json({
            error: 'Invalid access token'
          });
        }
        //check auth user is casting the right channel
        if (user.username == req.params.channel) {
          await dispatch(data, req.params.channel)
          return res.send('OK');
        }
        return res.status(403).json({
          error: 'Channel and username mismatch'
        });

      }
      return res.status(401).json({
        error: 'Access token required, but not found in request'
      });
    }

    await dispatch(data, req.params.channel)
    return res.send('OK');
  });

function dispatch(data, channel) {
  if (!channels[channel]) {
    initChannel(channel);
  }
  channels[channel]['socket'].emit('message', data);
}